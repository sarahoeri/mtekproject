package com.example.mtek;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IPinyWservice {
    @GET("/")
    Call<ServerStatus> getStatus();
}
