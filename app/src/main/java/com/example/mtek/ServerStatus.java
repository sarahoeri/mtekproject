package com.example.mtek;

public class ServerStatus {

    private String Group;
    private String Artifacts;
    private String Version;
    private String Status;

    public String getVersion() {
        return Version;
    }

    public String getStatus() {
        return Status;
    }

    public String getGroup() {
        return Group;
    }

    public String getArtifacts() {
        return Artifacts;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public void setGroup(String group) {
        Group = group;
    }

    public void setArtifacts(String artifacts) {
        Artifacts = artifacts;
    }

}
